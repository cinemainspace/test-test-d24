import logging
import uuid

import requests
from aiohttp import ClientSession
from asynctest import TestCase

from data import url_api

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


def generate_push_token():
    # Возвращает 32символьную шестнадцатеричную строку
    return uuid.uuid4().hex


class BaseTestCaseWithToken(TestCase):
    """
    Базовый класс для тестов
    """
    dev_type = 'W'  # По умолчанию получаем токен как от web cookie
    brand = 'YUT'

    @classmethod
    def setUpClass(cls):
        # Перед каждым классом тестов получаем токен для работы

        params = {
            'devType': cls.dev_type,
            'pushToken': generate_push_token(),
            'brand': cls.brand,
        }

        # Из-за особенностей asynctest делаем синхронно через requests

        with requests.Session() as session:
            response = session.post(url_api.POST_TOKEN, data=params)
            response.raise_for_status()

        cls.token = response.json()['auth_Token']
        log.debug('Токен получен: %s', cls.token)

    async def setUp(self):
        super().setUp()

        # raise_for_status -- проверяет статус на код ошибки
        # Вызовет исключение для последующих запросов со статусом 400 или выше
        # conn_timeout -- таймаут для установления соединения
        self.session = ClientSession(raise_for_status=True, conn_timeout=30)

    async def tearDown(self):
        await self.session.close()
