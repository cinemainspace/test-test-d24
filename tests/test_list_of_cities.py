import logging

from data import url_api
from tests.base_test_case import BaseTestCaseWithToken

log = logging.getLogger(__name__)

SCHEMA_KEYS_LIST_CITY = ['city_id', 'name', 'parent_name', 'utc']


class TestListOfCitiesAndroid(BaseTestCaseWithToken):
    """
    Тесты для устройства web cookie
    """

    async def test_post(self):
        """
        Тест POST: создание списка городов
        """
        payload = {'token': self.token}

        async with self.session.post(url_api.POST_CITY_LIST, data=payload) as response:
            response_json = await response.json()

        log.debug('response_json: %r', response_json)

        # Проверить, что для элемента в json приходят 4 параметра
        self.assertEqual(4, len(response_json[0]))

        # Проверить, что элементы(ключи) аналогичны по схеме
        # Но валидацию на схему лучше выполнять отдельных тестах на валидацию
        # self.assertListEqual(list(response_json[0].keys()), SCHEMA_KEYS_LIST_CITY)
        
        # Вывод городов
        log.info('List cities: %r', [city['name'] for city in response_json])

class TestListOfCitiesIOs(TestListOfCitiesAndroid):
    """
    Аналогичные кейсы, только токен получен как от устройства ios
    """
    dev_type = 'I'


class TestListOfCitiesWeb(TestListOfCitiesAndroid):
    """
    Аналогичные кейсы, только токен получен как от устройства android
    """
    dev_type = 'A'
